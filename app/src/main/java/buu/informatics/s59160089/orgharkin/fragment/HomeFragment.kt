package buu.informatics.s59160089.orgharkin.fragment


import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160089.orgharkin.fragment.HomeFragmentDirections
import buu.informatics.s59160089.orgharkin.R
import buu.informatics.s59160089.orgharkin.databinding.FragmentHomeBinding

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentHomeBinding>(inflater,
            R.layout.fragment_home, container , false)

        binding.logoImage.setImageResource(R.drawable.logo_project)
        binding.startBtn.setOnClickListener(){
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToRandomMenuFragment())

        }

        binding.historyBtn.setOnClickListener(){
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToHistory())
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        Log.i("HomeFragment Logging" , "HomeFragmentStarted")
    }

    override fun onPause() {
        super.onPause()
        Log.i("HomeFragment Logging" , "HomeFragmentPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("HomeFragment Logging" , "HomeFragmentStop")
    }

    override fun onResume() {
        super.onResume()
        Log.i("HomeFragment Logging" , "HomeFragmentResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("HomeFragment Logging" , "HomeFragmentDestroy")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("HomeFragment Logging" , "HomeFragmentCreate")
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_home_fragment, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }


}
