package buu.informatics.s59160089.orgharkin.fragment

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import buu.informatics.s59160089.orgharkin.database.HistoryDao
import buu.informatics.s59160089.orgharkin.database.HistoryListEntity
import kotlinx.coroutines.*
import kotlin.math.log

class ConfirmViewModel (private val dataBase : HistoryDao , application : Application) : AndroidViewModel(application) {
    private var numRandomMenu = MutableLiveData<Int>()
    private var types = MutableLiveData<String>()
    private var menuDessert = MutableLiveData<MutableList<String>>()
    private var menuFood = MutableLiveData<MutableList<String>>()
    private var viewModelJob = Job()
    private var uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    var allHistoryList = dataBase.getAll()

    private val _nameMenu = MutableLiveData<String>()
    val nameMenu : LiveData<String>
    get() = _nameMenu

    init {
        Log.i("ConfirmViewModel" , "ConfirmVieModel Created!")
        menuDessert.value = mutableListOf("Donut" , "Cake" , "ice-cream")
        menuFood.value = mutableListOf("spaghetti" , "pizza" , "papaya-salad")
        numRandomMenu.value = (0..2).random()
        _nameMenu.value = "Please push button random"

    }

    fun confirmMenu (nameMenu : String) {
        uiScope.launch {
            var newHis = HistoryListEntity()
            newHis.name = nameMenu
            insert(newHis)
        }
    }

    private suspend fun insert (menu : HistoryListEntity) {
        withContext(Dispatchers.IO) {
            dataBase.insert(menu)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
        Log.i("ConfirmViewModel" , "ConfirmVieModel Destroy!")
    }


    fun setType (stringType : String) {
        types.value = stringType
    }

    fun getMenuDessert() : MutableList<String>{
        return menuDessert.value!!
    }

    fun getMenuFood() : MutableList<String>{
        return menuFood.value!!
    }

    fun getTypes() : String{
       return types.value!!
    }

    fun getNumRandomMenu() : Int {
        return numRandomMenu.value!!
    }

    fun setNameMenu(newName : String) {
        _nameMenu.value = newName
    }

    private suspend fun clearDataSuspend() {
        withContext(Dispatchers.IO){
            dataBase.clear()
            allHistoryList = dataBase.getAll()
        }
    }

    fun clearDatabase() {
        uiScope.launch {
            clearDataSuspend()
        }
    }
}