package buu.informatics.s59160089.orgharkin.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
@Database (entities = [HistoryListEntity :: class] , version = 1 , exportSchema = false)
abstract class HistoryDatabase : RoomDatabase () {

    abstract val historyDababaseDao : HistoryDao

    companion object{
        @Volatile
        private var INSTANCE : HistoryDatabase? = null

        fun getInstace(context: Context) : HistoryDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if(instance == null) {
                    instance = Room.databaseBuilder(context.applicationContext , HistoryDatabase::class.java , "history")
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }

    }

}