package buu.informatics.s59160089.orgharkin.fragment


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import buu.informatics.s59160089.orgharkin.R
import buu.informatics.s59160089.orgharkin.databinding.FragmentAboutBinding

/**
 * A simple [Fragment] subclass.
 */
class About : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentAboutBinding>(inflater ,
            R.layout.fragment_about, container , false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        Log.i("AboutFragment Logging" , "AboutFragmentStarted")
    }

    override fun onPause() {
        super.onPause()
        Log.i("AboutFragment Logging" , "AboutFragmentPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("AboutFragment Logging" , "AboutFragmentStop")
    }

    override fun onResume() {
        super.onResume()
        Log.i("AboutFragment Logging" , "AboutFragmentResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("AboutFragment Logging" , "AboutFragmentDestroy")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("AboutFragment Logging" , "AboutFragmentCreate")
    }



}
