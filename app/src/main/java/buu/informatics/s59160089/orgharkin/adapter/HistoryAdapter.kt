package buu.informatics.s59160089.orgharkin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import buu.informatics.s59160089.orgharkin.R
import buu.informatics.s59160089.orgharkin.database.HistoryListEntity
//menuDessert.value = mutableListOf("Donut" , "Cake" , "ice-cream")
//menuFood.value = mutableListOf("spaghetti" , "pizza" , "papaya-salad")

class ItemViewHolder( itemView : View) : RecyclerView.ViewHolder(itemView){
    val menuName : TextView = itemView.findViewById(R.id.item_menu_name)
    val menuImage : ImageView = itemView.findViewById(R.id.item_menu_image)
}

class HistoryAdapter : RecyclerView.Adapter<ItemViewHolder>() {


    var data = listOf<HistoryListEntity>()
    set (value) {
        field = value
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_history , parent , false )
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = data[position]

        holder.menuName.text = item.name
        when( item.name){
            "Donut" -> holder.menuImage.setImageResource(R.drawable.donut_menu)
            "Cake" -> holder.menuImage.setImageResource(R.drawable.cake)
            "ice-cream" -> holder.menuImage.setImageResource(R.drawable.ice_cream)

            "spaghetti" -> holder.menuImage.setImageResource(R.drawable.spaghetti)
            "pizza" -> holder.menuImage.setImageResource(R.drawable.pizza)
            "papaya-salad" -> holder.menuImage.setImageResource(R.drawable.papaya)
        }
    }

}