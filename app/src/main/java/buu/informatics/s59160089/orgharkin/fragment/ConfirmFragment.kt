package buu.informatics.s59160089.orgharkin.fragment


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import buu.informatics.s59160089.orgharkin.fragment.ConfirmFragment
import buu.informatics.s59160089.orgharkin.fragment.ConfirmFragmentDirections
import buu.informatics.s59160089.orgharkin.R
import buu.informatics.s59160089.orgharkin.database.HistoryDatabase
import buu.informatics.s59160089.orgharkin.databinding.FragmentConfirmBinding

/**
 * A simple [Fragment] subclass.
 */

class ConfirmFragment : Fragment() {

    //private lateinit var viewModel : ConfirmViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = DataBindingUtil.inflate<FragmentConfirmBinding>(inflater ,
            R.layout.fragment_confirm, container , false)
        Log.i("ConfirmFragment" , "Call ViewModelProviders.of")
       // viewModel = ViewModelProviders.of(this).get(ConfirmViewModel::class.java)
        //database
        val application = requireNotNull(this.activity).application
        val dataSource = HistoryDatabase.getInstace(application).historyDababaseDao
        val viewModelFactory = ConfirmViewModelFactory(dataSource , application)
        val viewModel = ViewModelProviders.of(this , viewModelFactory).get(ConfirmViewModel::class.java)
        binding.comfirmViewModel
//---------------------------------------------------------------------------------------------------------------

        // set Type

        viewModel.setType(ConfirmFragmentArgs.fromBundle(arguments!!).type)

        // Inflate the layout for this fragment

        Toast.makeText(context , "${viewModel.getTypes()}" , Toast.LENGTH_SHORT).show()
        if(viewModel.getTypes() == "food"){
            binding.imageView.setImageResource(R.drawable.spaghetti)
        }else if(viewModel.getTypes() == "dessert"){
            binding.imageView.setImageResource(R.drawable.donut)
        }
        binding.confirmBtn.visibility = View.GONE

        viewModel.nameMenu.observe(viewLifecycleOwner, Observer {
            binding.nameOfMenu.text = it
        })

        viewModel.allHistoryList.observe(viewLifecycleOwner , Observer {
            for ( ei in it ){
                Log.i("GUM" , "id : ${ei.id} : name : ${ei.name}"  )
            }
        })

        binding.randomBtn.setOnClickListener(){
            if(viewModel.getTypes() == "food"){
                binding.confirmBtn.visibility = View.VISIBLE
                binding.randomBtn.visibility = View.GONE
                when(viewModel.getNumRandomMenu()){
                    0 -> {
                        binding.imageView.setImageResource(R.drawable.spaghetti)
                        viewModel.setNameMenu(viewModel.getMenuFood().get(0))
//                        viewModel.nameMenu.observe(this, Observer {
//                            binding.nameOfMenu.text = it
//                        })
                    }
                    1 -> {
                        binding.imageView.setImageResource(R.drawable.pizza)
                        viewModel.setNameMenu(viewModel.getMenuFood().get(1))
//                        viewModel.nameMenu.observe(this, Observer {
//                            binding.nameOfMenu.text = it
//                        })
                    }
                    2 -> {
                        binding.imageView.setImageResource(R.drawable.papaya)
                        viewModel.setNameMenu(viewModel.getMenuFood().get(2))
//                        viewModel.nameMenu.observe(this, Observer {
//                            binding.nameOfMenu.text = it
//                        })
                    }
                }
            }else if ( viewModel.getTypes() == "dessert"){
                binding.confirmBtn.visibility = View.VISIBLE
                binding.randomBtn.visibility = View.GONE
                when(viewModel.getNumRandomMenu()){
                    0 -> {
                        binding.imageView.setImageResource(R.drawable.donut)
                        viewModel.setNameMenu(viewModel.getMenuDessert().get(0))
//                        viewModel.nameMenu.observe(this, Observer {
//                            binding.nameOfMenu.text = it
//                        })
                    }
                    1 -> {
                        binding.imageView.setImageResource(R.drawable.cake)
                        viewModel.setNameMenu(viewModel.getMenuDessert().get(1))
//                        viewModel.nameMenu.observe(this, Observer {
//                            binding.nameOfMenu.text = it
//                        })
                    }
                    2 -> {
                        binding.imageView.setImageResource(R.drawable.ice_cream)
                        viewModel.setNameMenu(viewModel.getMenuDessert().get(2))
//                        viewModel.nameMenu.observe(this, Observer {
//                            binding.nameOfMenu.text = it
//                        })
                    }
                }

            }
        }

        binding.confirmBtn.setOnClickListener(){

            viewModel.confirmMenu(viewModel.nameMenu.value!!)

            findNavController().navigate(ConfirmFragmentDirections.actionConfirmFragmentToHistory())
        }

        binding.cancelBtn.setOnClickListener(){
//            viewModel.clearDatabase()
            findNavController().navigate(ConfirmFragmentDirections.actionConfirmFragmentToRandomMenuFragment())
        }


        binding.lifecycleOwner = this
        return binding.root
    }



    override fun onStart() {
        super.onStart()
        Log.i("ConfirmFragment Logging" , "ConfirmFragmentStarted")
    }

    override fun onPause() {
        super.onPause()
        Log.i("ConfirmFragment Logging" , "ConfirmFragmentPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("ConfirmFragment Logging" , "ConfirmFragmentStop")
    }

    override fun onResume() {
        super.onResume()
        Log.i("ConfirmFragment Logging" , "ConfirmFragmentResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("ConfirmFragment Logging" , "ConfirmFragmentDestroy")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("ConfirmFragment Logging" , "ConfirmFragmentCreate")
    }




}
