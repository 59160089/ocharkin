package buu.informatics.s59160089.orgharkin.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update


@Dao
interface HistoryDao {
    @Insert
    fun insert(menuName:HistoryListEntity)

    @Update
    fun update(newMenuName : HistoryListEntity)

    @Query ("SELECT * FROM history WHERE id = :key")
    fun get(key : Long) : HistoryListEntity

    @Query ("DELETE FROM history")
    fun clear()

    @Query ("SELECT * FROM history ORDER BY id ASC")
    fun getAll() : LiveData<List<HistoryListEntity>>

    @Query ( "SELECT * FROM history LIMIT 1")
    fun getOne() : LiveData<HistoryListEntity>
}