package buu.informatics.s59160089.orgharkin.fragment

import android.app.Application
import android.content.Intent
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import buu.informatics.s59160089.orgharkin.R
import buu.informatics.s59160089.orgharkin.database.HistoryDao
import kotlinx.coroutines.*

class HistoryViewModel (private val dataBase : HistoryDao, application : Application) : AndroidViewModel(application) {
    var allHistoryList = dataBase.getAll()
    private var viewModelJob = Job()
    private var uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private suspend fun clearDataSuspend() {
        withContext(Dispatchers.IO){
            dataBase.clear()
            allHistoryList = dataBase.getAll()
        }
    }

    fun clearDatabase() {
        uiScope.launch {
            clearDataSuspend()
        }

    }


}