package buu.informatics.s59160089.orgharkin.fragment


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import buu.informatics.s59160089.orgharkin.R
import buu.informatics.s59160089.orgharkin.fragment.RandomMenuFragmentDirections
import buu.informatics.s59160089.orgharkin.databinding.FragmentRandomMenuBinding

/**
 * A simple [Fragment] subclass.
 */
class RandomMenuFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentRandomMenuBinding>(inflater,
            R.layout.fragment_random_menu, container ,false )
        // Inflate the layout for this fragment
        binding.menuFoodBtn.setOnClickListener(){
            findNavController().navigate(
                RandomMenuFragmentDirections.actionRandomMenuFragmentToConfirmFragment(
                    "food"
                )
            )
        }
        binding.menuDessertBtn.setOnClickListener(){
            findNavController().navigate(
                RandomMenuFragmentDirections.actionRandomMenuFragmentToConfirmFragment(
                    "dessert"
                )
            )
        }
        return binding.root
    }



    override fun onStart() {
        super.onStart()
        Log.i("RandomFragment Logging" , "RandomMenuFragmentStarted")
    }

    override fun onPause() {
        super.onPause()
        Log.i("RandomFragment Logging" , "RandomMenuFragmentPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("RandomFragment Logging" , "RandomMenuFragmentStop")
    }

    override fun onResume() {
        super.onResume()
        Log.i("RandomFragment Logging" , "RandomMenuFragmentResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("RandomFragment Logging" , "RandomMenuFragmentDestroy")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("RandomFragment Logging" , "RandomMenuFragmentCreate")
    }





}
