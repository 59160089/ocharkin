package buu.informatics.s59160089.orgharkin.fragment

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.informatics.s59160089.orgharkin.database.HistoryDao
import java.lang.IllegalArgumentException

class ConfirmViewModelFactory (private val dataSource : HistoryDao, private val application : Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ConfirmViewModel ::class.java)){
            return ConfirmViewModel(dataSource , application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}