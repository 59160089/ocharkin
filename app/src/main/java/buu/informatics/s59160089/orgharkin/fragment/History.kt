package buu.informatics.s59160089.orgharkin.fragment


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import buu.informatics.s59160089.orgharkin.R
import buu.informatics.s59160089.orgharkin.database.HistoryDatabase
import buu.informatics.s59160089.orgharkin.databinding.FragmentHistoryBinding
import buu.informatics.s59160089.orgharkin.adapter.HistoryAdapter as HistoryAdapter1

/**
 * A simple [Fragment] subclass.
 */
class History : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentHistoryBinding>(inflater ,
            R.layout.fragment_history, container , false)
        // Inflate the layout for this fragment

        val application = requireNotNull(this.activity).application
        val dataSource = HistoryDatabase.getInstace(application).historyDababaseDao
        val viewModelFactory = HistoryViewModelFactory(dataSource , application)
        val viewModel = ViewModelProviders.of(this , viewModelFactory).get(HistoryViewModel::class.java)
        binding.historyViewModel = viewModel
        binding.setLifecycleOwner (this)
        setHasOptionsMenu(true)

        val adapter = HistoryAdapter1()
        binding.recViewHistory.adapter = adapter

        viewModel.allHistoryList.observe(viewLifecycleOwner , Observer {
            it.let {
                adapter.data = it
            }
            for ( ei in it ){
                Log.i("historyLog" , "id : ${ei.id} : name : ${ei.name}"  )
            }
        })

        return binding.root

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item!!.itemId){
            R.id.share -> startActivity(getSharedIntent())
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.shared, menu)
    }

    private fun getSharedIntent () : Intent{
        val sharedIntent = Intent(Intent.ACTION_SEND)
        sharedIntent.setType("text/plain").putExtra(Intent.EXTRA_TEXT , getString(R.string.aboutText))
        return sharedIntent
    }



    override fun onStart() {
        super.onStart()
        Log.i("HistoryFragment Logging" , "HistoryFragmentStarted")
    }

    override fun onPause() {
        super.onPause()
        Log.i("HistoryFragment Logging" , "HistoryFragmentPause")
    }

    override fun onStop() {
        super.onStop()
        Log.i("HistoryFragment Logging" , "HistoryFragmentStop")
    }

    override fun onResume() {
        super.onResume()
        Log.i("HistoryFragment Logging" , "HistoryFragmentResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("HistoryFragment Logging" , "HistoryFragmentDestroy")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i("HistoryFragment Logging" , "HistoryFragmentCreate")
    }



}
