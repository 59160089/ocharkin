package buu.informatics.s59160089.orgharkin.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "history")
data class HistoryListEntity (
    @PrimaryKey(autoGenerate = true)
    var id : Long =  0L,
    @ColumnInfo(name = "menuName")
    var name : String = ""
)